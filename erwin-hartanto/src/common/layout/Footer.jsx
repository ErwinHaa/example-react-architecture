import styles from './Footer.module.scss';

const Footer = () => {
  return (
    <footer className={styles.footer}>
      &copy; Copyright Erwin Hartanto, 2021
    </footer>
  );
};

export default Footer;
