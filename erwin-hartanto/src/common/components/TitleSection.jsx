import styles from './TitleSection.module.scss';

const TitleSection = ({ title, icon }) => {
  return (
    <header className={styles.titleSection}>
      {icon}
      <h1 className={styles.titleText}>{title}</h1>
    </header>
  );
};

export default TitleSection;
