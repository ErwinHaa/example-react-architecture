import { useState, useRef, useEffect } from 'react';
import { IoSearchOutline } from 'react-icons/io5';
import styles from './Search.module.scss';

const Search = ({ item, className, value, onChange, onSubmit }) => {
  const [focused, setFocused] = useState(false);

  const boxRef = useRef();

  useEffect(() => {
    const onBodyClick = (e) => {
      if (boxRef.current.contains(e.target)) return;
      setFocused(false);
    };

    document.body.addEventListener('click', onBodyClick, { capture: true });

    return () => {
      document.body.removeEventListener('click', onBodyClick, {
        capture: true,
      });
    };
  }, []);

  const handleFocus = () => {
    setFocused(true);
  };

  return (
    <form className={className} onSubmit={onSubmit}>
      <div
        ref={boxRef}
        className={`${styles.searchIconBox} ${
          focused && styles['searchIconBox--focused']
        }`}>
        <IoSearchOutline className={styles.icon} />
        <input
          className={styles.input}
          type="text"
          placeholder={`Search ${item}`}
          onFocus={handleFocus}
          value={value}
          onChange={onChange}
        />
      </div>
    </form>
  );
};

export default Search;
