import axios from 'axios';

const baseURL = 'https://60abbadc5a4de40017ccac0c.mockapi.io/api/v1/todos';

export const getAllTodos = async () => {
  const response = await axios.get(baseURL);

  return response.data;
};

export const postTodo = async (todo) => {
  const { title, content } = todo;
  const response = await axios.post(baseURL, { title, content });

  return response.data;
};

export const deleteTodo = async (todoId) => {
  const response = await axios.delete(`${baseURL}/${todoId}`);

  return response.data;
};
