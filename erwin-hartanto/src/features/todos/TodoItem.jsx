import { IoChevronForwardOutline, IoTrashBin } from 'react-icons/io5';
import { useSelector, useDispatch } from 'react-redux';

import { selectTodoById, deleteTodoById } from './todosSlice';

import styles from './TodoItem.module.scss';

const TodoItem = ({ todoId }) => {
  const dispatch = useDispatch();
  const todo = useSelector((state) => selectTodoById(state, todoId));

  return (
    <li className={styles.todoItem}>
      <IoChevronForwardOutline className={styles.icon} />
      <p className={styles.title}>{todo.title}</p>
      <IoTrashBin
        className={styles.icon}
        onClick={() => dispatch(deleteTodoById(todoId))}
      />
    </li>
  );
};

export default TodoItem;
