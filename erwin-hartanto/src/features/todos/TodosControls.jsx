import AddNewTodo from './AddNewTodo';
import styles from './TodosControls.module.scss';

const TodosControls = () => {
  return (
    <div className={styles.todosControls}>
      <h2 className={styles.title}>Add a new todo</h2>
      <AddNewTodo />
    </div>
  );
};

export default TodosControls;
