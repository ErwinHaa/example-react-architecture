import { useEffect } from 'react';
import { useDispatch } from 'react-redux';

import TodosList from './TodosList';
import TodosControls from './TodosControls';

import { fetchAllTodos } from './todosSlice';

import styles from './Todos.module.scss';
import TodoHeader from './TodoHeader';

const Todos = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchAllTodos());
  }, [dispatch]);

  return (
    <main className={styles.todos}>
      <TodoHeader />
      <TodosList />
      <TodosControls />
    </main>
  );
};

export default Todos;
