import { useState } from 'react';
import { useDispatch } from 'react-redux';

import { addNewTodo } from './todosSlice';

import styles from './AddNewTodo.module.scss';

const AddNewTodo = () => {
  const [title, setTitle] = useState('');
  const [content, setContent] = useState('');

  const dispatch = useDispatch();

  const handleButtonClick = () => {
    dispatch(addNewTodo({ title, content }));
  };

  return (
    <form className={styles.addNewTodo}>
      <div className={styles.inputBox}>
        <div className={styles.formControl}>
          <label htmlFor="todoTitle">Title</label>
          <input
            type="text"
            className={styles.input}
            id="todoTitle"
            placeholder="Input Title"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
          />
          <div className={styles.line} />
        </div>
        <div className={styles.formControl}>
          <label htmlFor="todoContent">Content</label>
          <input
            type="text"
            className={styles.input}
            id="todoContent"
            placeholder="Input Content"
            value={content}
            onChange={(e) => setContent(e.target.value)}
          />
          <div className={styles.line} />
        </div>
      </div>
      <button
        onClick={handleButtonClick}
        type="button"
        className={styles.submitBtn}>
        Add Todo
      </button>
    </form>
  );
};

export default AddNewTodo;
