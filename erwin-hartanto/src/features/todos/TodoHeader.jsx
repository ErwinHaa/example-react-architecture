import Header from '../../common/layout/Header';
import TodoSearch from './TodoSearch';

import styles from './TodoHeader.module.scss';

const TodoHeader = () => {
  return (
    <Header className={styles.todoHeader}>
      <TodoSearch />
    </Header>
  );
};

export default TodoHeader;
