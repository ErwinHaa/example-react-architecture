import { createSelector } from 'reselect';
import { getAllTodos, postTodo, deleteTodo } from '../../services/apis/mockapi';

export const STATUS = {
  IDLE: 'idle',
  LOADING: 'loading',
  FULFILLED: 'fulfilled',
  REJECTED: 'rejected',
};

export const todosTypes = {
  fetchTodosStart: 'todos/fetchAllTodosStart',
  fetchTodosFulfilled: 'todos/fetchAllTodosFulfilled',
  fetchTodosRejected: 'todos/fetchAllTodosRejected',
  addNewTodo: 'todos/addNewTodo',
  removeTodo: 'todos/removeTodo',
};

const initialState = {
  todos: [],
  status: STATUS.IDLE,
  error: undefined,
};

const todosReducer = (state = initialState, action) => {
  switch (action.type) {
    case todosTypes.fetchTodosStart:
      return { ...state, todos: [], status: STATUS.LOADING, error: undefined };
    case todosTypes.fetchTodosFulfilled:
      return { ...state, todos: action.payload, status: STATUS.FULFILLED };
    case todosTypes.fetchTodosRejected:
      return { ...state, status: STATUS.REJECTED, error: action.error };
    case todosTypes.addNewTodo:
      return { ...state, todos: [...state.todos, action.payload] };
    case todosTypes.removeTodo:
      const existingTodo = state.todos.find(
        (todo) => todo.id === action.payload
      );
      if (existingTodo) {
        return {
          ...state,
          todos: state.todos.filter((todo) => todo.id !== existingTodo.id),
        };
      }

      return state;
    default:
      return state;
  }
};

export const fetchAllTodosStart = () => {
  return { type: todosTypes.fetchTodosStart };
};

export const fetchAllTodosFulfilled = (todos) => {
  return { type: todosTypes.fetchTodosFulfilled, payload: todos };
};

export const fetchAllTodosRejected = (error) => {
  return { type: todosTypes.fetchTodosRejected, error };
};

export const addTodo = (todo) => {
  return { type: todosTypes.addNewTodo, payload: todo };
};

export const removeTodo = (todoId) => {
  return { type: todosTypes.removeTodo, payload: todoId };
};

export const fetchAllTodos = () => async (dispatch) => {
  dispatch(fetchAllTodosStart());

  try {
    const todos = await getAllTodos();

    dispatch(fetchAllTodosFulfilled(todos));
  } catch (err) {
    dispatch(fetchAllTodosRejected(err.message));
  }
};

export const addNewTodo = (todo) => async (dispatch) => {
  const newTodo = await postTodo(todo);

  dispatch(addTodo(newTodo));
};

export const deleteTodoById = (todoId) => async (dispatch) => {
  await deleteTodo(todoId);

  dispatch(removeTodo(todoId));
};

export const selectAllTodos = (state) => state.todos.todos;
// export const selectTodosIds = createSelector(selectAllTodos, (todos) =>
//   todos.map((todo) => todo.id)
// );
// export const selectTodoById = createSelector(
//   [
//     selectAllTodos,
//     (_, todoId) => {
//       return todoId;
//     },
//   ],
//   (todos, todoId) => {
//     const existingTodo = todos.find((todo) => todo.id === todoId);

//     if (existingTodo) {
//       return existingTodo;
//     }
//   }
// );

export const selectFilteredTodos = createSelector(
  [selectAllTodos, (state) => state.filters.term],
  (todos, term) => todos.filter((todo) => todo.title.includes(term))
);

export const selectFilteredTodoIds = createSelector(
  selectFilteredTodos,
  (todos) => todos.map((todo) => todo.id)
);

export const selectTodoById = createSelector(
  [
    selectAllTodos,
    (_, todoId) => {
      return todoId;
    },
  ],
  (todos, todoId) => {
    const existingTodo = todos.find((todo) => todo.id === todoId);

    if (existingTodo) {
      return existingTodo;
    }
  }
);

export const selectTodosStatus = (state) => state.todos.status;

export default todosReducer;
