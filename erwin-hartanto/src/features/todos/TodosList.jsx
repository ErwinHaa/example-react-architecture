import React from 'react';
import { useSelector } from 'react-redux';
import TodoItem from './TodoItem';

import { selectFilteredTodoIds, selectTodosStatus, STATUS } from './todosSlice';

import styles from './TodosList.module.scss';

const TodosList = () => {
  const todoIds = useSelector(selectFilteredTodoIds);
  const status = useSelector(selectTodosStatus);

  if (status === STATUS.LOADING) {
    return (
      <div className={styles.todosList}>
        <h1 className={styles.loadingText}>Loading...</h1>
      </div>
    );
  }

  if (!todoIds.length) {
    return (
      <div className={styles.todosList}>
        <h1 className={styles.notFoundText}>No Todos Found</h1>
      </div>
    );
  }

  const renderedTodoItems = todoIds.map((todoId) => (
    <TodoItem key={todoId} todoId={todoId} />
  ));

  return <ul className={styles.todosList}>{renderedTodoItems}</ul>;
};

export default TodosList;
