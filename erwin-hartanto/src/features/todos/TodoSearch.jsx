import { useState } from 'react';
import { useDispatch } from 'react-redux';

import { addSearchTerm } from '../filters/filtersSlice';

import Search from '../../common/components/Search';

import styles from './TodoSearch.module.scss';

const TodoSearch = () => {
  const [term, setTerm] = useState('');

  const dispatch = useDispatch();

  const handleTermChange = (e) => {
    setTerm(e.target.value);
  };

  const handleSearchSubmit = (e) => {
    e.preventDefault();
    dispatch(addSearchTerm(term));
  };

  return (
    <Search
      item="todos"
      className={styles.todoSearch}
      onChange={handleTermChange}
      value={term}
      onSubmit={handleSearchSubmit}
    />
  );
};

export default TodoSearch;
