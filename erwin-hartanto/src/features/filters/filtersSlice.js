const filterTypes = {
  searchTermAdded: 'filters/searchTermAdded',
};

const initialState = {
  term: '',
};

const filtersReducer = (state = initialState, action) => {
  switch (action.type) {
    case filterTypes.searchTermAdded:
      return { ...state, term: action.payload };
    default:
      return state;
  }
};

export const addSearchTerm = (term) => {
  return { type: filterTypes.searchTermAdded, payload: term };
};

export default filtersReducer;
