import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import { store } from './app/store';

import './assets/sass/main.scss';

import App from './app/App';

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.querySelector('#root')
);
