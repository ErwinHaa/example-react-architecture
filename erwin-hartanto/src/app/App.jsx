import { IoListOutline } from 'react-icons/io5';

import TitleSection from '../common/components/TitleSection';
import Todos from '../features/todos/Todos';
import Footer from '../common/layout/Footer';

import styles from './App.module.scss';

const App = () => {
  return (
    <div className={styles.container}>
      <TitleSection title="Todos" icon={<IoListOutline />} />
      <Todos />
      <Footer />
    </div>
  );
};

export default App;
