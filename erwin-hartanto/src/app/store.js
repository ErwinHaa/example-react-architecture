import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import { rootReducer } from './rootReducer';

const storeEnhancers = applyMiddleware(thunk);

const composedEnhancers = composeWithDevTools(storeEnhancers);

export const store = createStore(rootReducer, composedEnhancers);
